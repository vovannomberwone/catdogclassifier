mean = [0.485, 0.456, 0.406]
std  = [0.229, 0.224, 0.225]

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from PIL import Image

import torch
import torch.nn as nn
import torchvision

from torchvision import transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn.functional as F

import glob
import os

from torch.utils.tensorboard import SummaryWriter

image_size = (100, 100)
image_row_size = image_size[0] * image_size[1]

class CatDogDataset(Dataset):
    def __init__(self, path, transform=None):
        self.classes   = ['cats', 'dogs'] 
        self.path      = [f"{path}/{className}" for className in self.classes]
        self.file_list = [glob.glob(f"{x}/*") for x in self.path]
        self.transform = transform
        
        files = []
        for i, className in enumerate(self.classes):
            for fileName in self.file_list[i]:
                files.append([className, fileName])
        self.file_list = files
        files = None
        
    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):
        fileName = self.file_list[idx][1]
        if self.file_list[idx][0] == 'dogs':
        	classCategory = 0
        else:
        	classCategory = 1
        im = Image.open(fileName)
        if self.transform:
            im = self.transform(im)
        return im, classCategory

def imshow(source):
    plt.figure(figsize = (10, 10))
    imt = (source.view(-1, image_size[0], image_size[0]))
    imt = imt.numpy().transpose([1, 2, 0])
    imt = (std * imt + mean).clip(0,1)
    plt.subplot(1, 2, 2)
    plt.imshow(imt)
    plt.show()

class MyCNNModel(torch.nn.Module):
	def __init__(self, batch_size):
		super(MyCNNModel, self).__init__()
		self.batch_size = batch_size
		self.relu       = torch.nn.ReLU()
		self.conv1      = torch.nn.Conv2d(3, 16, 7, stride = 1, padding = 0) 
		self.bn1        = torch.nn.BatchNorm2d(16)
		self.pad1       = torch.nn.MaxPool2d(2, stride = 2)
		self.conv2      = torch.nn.Conv2d(16, 16, 6, stride = 1, padding = 0)
		self.bn2        = torch.nn.BatchNorm2d(16)
		self.pad2       = torch.nn.MaxPool2d(2, stride = 2)
		self.conv3      = torch.nn.Conv2d(16, 16, 9, stride = 1, padding = 0)
		self.bn3        = torch.nn.BatchNorm2d(16)
		self.conv4      = torch.nn.Conv2d(16, 16, 6, stride = 1, padding = 0)
		self.bn4        = torch.nn.BatchNorm2d(16)
		self.fc1        = torch.nn.Linear(in_features = 8 * 8 * 16, out_features = 2)

	def forward(self, input):
		out = self.conv1(input)
		out = self.bn1(out)
		out = self.relu(out)
		out = self.pad1(out)
		out = self.conv2(out)
		out = self.bn2(out)
		out = self.relu(out)
		out = self.pad2(out)
		out = self.conv3(out)
		out = self.bn3(out)
		out = self.relu(out)
		out = self.conv4(out)
		out = self.bn4(out)
		out = self.relu(out)
		out = self.fc1(out.view(self.batch_size, -1))
		return out

class Params():
	def __init__(
		self, do_bn, fc_sizes, b_size, learning_rate, mmm, epochs,
		loss_func, act_funcs
	):
		self.do_bn = do_bn
		self.N_hidden = len(fc_sizes) - 1
		self.fc_sizes = fc_sizes
		self.b_size = b_size
		self.learning_rate = learning_rate
		self.mmm = mmm
		self.epochs = epochs
		self.loss_func = loss_func
		self.act_funcs = act_funcs

class FCNet(nn.Module):
	def __init__(self, par):
		super(FCNet, self).__init__()
		self.par = par
		self.fcs = []
		self.bns = []
		for i in range(par.N_hidden):
			fc = nn.Linear(par.fc_sizes[i], par.fc_sizes[i + 1])
			setattr(self, 'fc%i' % i, fc)
			self.fcs.append(fc)
			if (par.do_bn and i < par.N_hidden - 1):
				bn = nn.BatchNorm1d(par.fc_sizes[i + 1])
				setattr(self, 'bn%i' % i, bn)
				self.bns.append(bn)
	
	def forward(self, x):
		for i in range(self.par.N_hidden):
			x = self.fcs[i](x)
			if (self.par.do_bn) and (i < self.par.N_hidden - 1):
				x = self.bns[i](x)
			x = self.par.act_funcs[i](x)
		return x
	
	def TRAIN(self, dataloader_train_train):
		
		optimizer = torch.optim.SGD(
			self.parameters(), lr = self.par.learning_rate,
			momentum = self.par.mmm
		)
		
		set_len = len(dataloader_train_train) * self.par.b_size
		for epoch in range(self.par.epochs): 
			self.train()
			accuracy = 0
			for batch_idx, (X, Y) in enumerate(dataloader_train_train):
				optimizer.zero_grad()
				data = X
				target = Y
				FCNet_out = self(data)
				loss = self.par.loss_func(FCNet_out, target)
				loss.backward()
				optimizer.step()

				accuracy_batch = 0
				for j in range (self.par.b_size):
					if FCNet_out[j][0] > FCNet_out[j][1]:
						class_res = 0
					else:
						class_res = 1
					accuracy_batch += (class_res == Y[j].item())
					accuracy += (class_res == Y[j].item())
				
			print('epoch:', epoch, 'accuracy:', (float)(accuracy) / set_len)

class CatDogDataset2(Dataset):
    def __init__(self, path, transform=None):
        self.classes   = ['cats', 'dogs'] 
        self.path      = [f"{path}/{className}" for className in self.classes]
        self.file_list = [glob.glob(f"{x}/*") for x in self.path]
        self.transform = transform
        
        files = []
        for i, className in enumerate(self.classes):
            for fileName in self.file_list[i]:
                files.append([className, fileName])
        self.file_list = files
        files = None
        
    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):
        fileName = self.file_list[idx][1]
        if self.file_list[idx][0] == 'dogs':
        	classCategory = 0
        else:
        	classCategory = 1
        im = Image.open(fileName)
        if self.transform:
            im = self.transform(im)
        return im.reshape(-1), classCategory

def identical_func(x):
	return x


def main():

	transform = transforms.Compose([
		transforms.Resize(image_size),
		transforms.ToTensor(),
		transforms.Normalize(mean, std)
	])
	path_train    = './data/training_set'
	dataset_train = CatDogDataset(path_train, transform = transform)
	path_val      = './data/test_set'
	dataset_val   = CatDogDataset(path_train, transform = transform)
	
	criterion = torch.nn.CrossEntropyLoss()

	batch_size = 50

	dataloader_train = DataLoader (
		dataset = dataset_train,
		shuffle = False,
		batch_size = batch_size, 
		num_workers = 0,
		drop_last = True
	)
	
	dataloader_val = DataLoader (
		dataset = dataset_val,
		shuffle = False,
		batch_size = batch_size, 
		num_workers = 0,
		drop_last = True
	)
	'''
	dataset2 = CatDogDataset2(path, transform = transform)
	dataloader_train2 = dataloader_train (
		dataset = dataset2, 
		shuffle = False,
		batch_size = batch_size, 
		num_workers = 0,
		drop_last = True
	)

	par = Params(
		do_bn = True, fc_sizes = [image_row_size * 3, 100, 100, 100, 2],
		b_size = 100, learning_rate = 0.05, mmm = 0, epochs = 20,
		loss_func = criterion,
		act_funcs = [torch.relu, torch.relu, torch.relu, identical_func]
	)
	FCNet2 = FCNet(par)
	FCNet2.TRAIN(dataloader_train2)

	print('-------------------------')
	'''
	
	cnn_net = MyCNNModel(batch_size)
	optimizer = torch.optim.SGD(cnn_net.parameters(), lr = 0.001, momentum = 0.90)
	len_train = len(dataloader_train) * batch_size
	len_val = len(dataloader_val) * batch_size

	writer = SummaryWriter()
	images, labels = next(iter(dataloader_train))
	writer.add_graph(cnn_net, images)

	epochs = 20
	for epoch in range(epochs):
		
		# train
		accuracy_train = 0
		cnn_net.train()
		for i, (X, Y) in enumerate(dataloader_train):
			optimizer.zero_grad()
			inference = cnn_net.forward(X)
			loss = criterion(inference, Y)
			
			for j in range (batch_size):
				if inference[j][0] > inference[j][1]:
					class_res = 0
				else:
					class_res = 1
				accuracy_train += (class_res == Y[j].item())
			
			loss.backward()
			optimizer.step()
		
		# validate
		accuracy_val = 0
		cnn_net.eval()
		for i, (X, Y) in enumerate(dataloader_val):
			inference = cnn_net.forward(X)
			loss = criterion(inference, Y)
			
			for j in range (batch_size):
				if inference[j][0] > inference[j][1]:
					class_res = 0
				else:
					class_res = 1
				accuracy_val += (class_res == Y[j].item())
			
		writer.add_scalar('accuracy_val', (float)(accuracy_val) / len_val, global_step=epoch, walltime=None)
		writer.add_scalar('accuracy_train', (float)(accuracy_train) / len_train, global_step=epoch, walltime=None)
		print('epoch:', epoch, 'accuracy_val:', (float)(accuracy_val) / len_val, 'accuracy_train:', (float)(accuracy_train) / len_train)
	
	writer.close()

if __name__ == '__main__':
	main()